/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

function SEO({ description, lang, meta, keywords, title, canonicalLink }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author,
            canonicalLink
          }
        }
      }
    `
  )

  const cLink = canonicalLink || site.siteMetadata.canonicalLink;
  const metaDescription = description || site.siteMetadata.description

  return (
    <Helmet
      htmlAttributes={{
        lang,
        dir: `ltr`
      }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          property: `og:url`,
          content: cLink
        },
        {
          property: `og:site_name`,
          content: `iamcollins`
        },
        {
          property: `og:locale`,
          content: `en_GB`
        },
        {
          property: `og:image`,
          content: `/icons/icon-48x48.png`
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: site.siteMetadata.author,
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:url`,
          content: cLink
        },
        {
          name: `twitter:image`,
          content: `/icons/icon-48x48.png`
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
        {
          name: `apple-mobile-web-app-capable`,
          content: `yes`
        },
        {
          name: `apple-mobile-web-app-status-bar-style`,
          content: `black`
        }
      ]
        .concat(
          keywords.length > 0
            ? {
                name: `keywords`,
                content: keywords.join(`, `),
              }
            : []
        )
        .concat(meta)}
    >
    
      <link rel="canonical" href={cLink} />
      <link rel="dns-prefetch" href="https://iamcollins.dev"></link>
      <link rel="preconnect" href="https://iamcollins.dev"></link>
    </Helmet>
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  keywords: [],
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.array,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
  canonicalLink: PropTypes.string
}

export default SEO
