import React from "react"
import styles from "../styles/socialIcons.module.scss"

import twitterImage from "../images/twitter.png"
import linkedInImage from "../images/linkedin.png"
import stackOverFlowImage from "../images/stackoverflow.png"
import instagramImage from "../images/instagram.png"

const SocialIcons = () => (
    <React.Fragment>
        <section className={styles.container}>
            <a href="https://twitter.com/iamcollinsZA" target="_blank" rel="noopener noreferrer">
                <img src={twitterImage} className={styles.twitter} alt="Twitter Icon"/>
            </a>
            <a href="https://www.instagram.com/iamcollinsza/" target="_blank" rel="noopener noreferrer">
                <img src={instagramImage} className={styles.instagram} alt="Instagram Icon"/>
            </a>
            <a href="https://www.linkedin.com/in/llewellyncollins/" target="_blank" rel="noopener noreferrer">
                <img src={linkedInImage} className={styles.linkedIn} alt="LinkedIn Icon"/>
            </a>
            <a href="https://stackoverflow.com/users/story/737997" target="_blank" rel="noopener noreferrer">
                <img src={stackOverFlowImage} className={styles.stackOverFlow} alt="StackOverflow Icon"/>
            </a>
        </section>
    </React.Fragment>
)

export default SocialIcons
