import React from "react"
import ProfileIcon from "./profileIcon"
import Menu from "./menu"
import SocialIcons from "./socialIcons"
import styles from "../styles/sidebar.module.scss"

const SideBar = () => (
    <React.Fragment>
        <aside className={styles.aside}>
            <header>
                <ProfileIcon />
                <h2>Llewellyn Collins</h2>
            </header>
            <Menu/>
            <SocialIcons/>
        </aside>
    </React.Fragment>
)

export default SideBar
