import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styles from "../styles/menu.module.scss"

const Menu = ({ siteTitle }) => (
    <nav className={styles.nav}>
        <ul>
            <li><Link to="/" activeClassName={styles.active}>About</Link></li>
            <li><Link to="/blog"activeClassName={styles.active}>Blog</Link></li>
            <li><Link to="/projects" activeClassName={styles.active}>Projects</Link></li>
        </ul>
    </nav>
)

Menu.propTypes = {
    siteTitle: PropTypes.string,
}

Menu.defaultProps = {
    siteTitle: ``,
}

export default Menu
