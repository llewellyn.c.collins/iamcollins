import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

export default class ProfileIcon extends React.Component {
    render() {
        return (
            <StaticQuery
                query={graphql`
                    query {
                        imageOne: file(relativePath: {eq: "profile_pic.jpg"}) {
                            childImageSharp {
                                fluid(maxWidth: 500) {
                                ...GatsbyImageSharpFluid
                                }
                            }
                        }
                    }
                `}
                render={data => {
                    return (
                        <Img fluid={data.imageOne.childImageSharp.fluid} fadeIn alt="Profile Icon"/>
                    )
                }}
            />
        )
    }
}