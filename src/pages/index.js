import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import Layout from "../layouts/layout"
import SEO from "../components/seo"
import styles from "../styles/index.module.scss"

const IndexPage = ({ data }) => {
  
  const pageData = data.prismicHomepage.data;
  const stacks = pageData.body.filter((item)=>{
    return !!item.primary;
  });
  let historyItems = pageData.body.filter((item)=>{
    return !item.primary;
  });
  historyItems = historyItems.reduce((items, item)=>{
    items.push(item.items[0]);
    return items;
  }, []);
  return (
    <React.Fragment>
      <SEO title="about"/>
      <Layout>
          <header className={styles.header}>
              <Img fluid={pageData.featured_image.localFile.childImageSharp.fluid} fadeIn className={styles.image} alt={pageData.featured_image.alt}/>
              <section className={styles.about}>
                <h4>Software Creator</h4>
                <h2>Llewellyn Collins</h2>
                <div dangerouslySetInnerHTML={{ __html: pageData.intro.html }}/>
                <a href={pageData.cv.url} target="_blank" rel="noopener noreferrer">Download CV</a>
              </section>
          </header>
          <section className={styles.section}>
            <h1>Resume</h1>
            <div className={styles.resume}>
              <div className={styles.experience}>
                <h2>Experience</h2>
                {
                 historyItems.map((item, index)=>{
                    return (
                      <div className={styles.content} key={index}>
                        <div className={styles.leftSide}>
                          <div className={styles.date}>{item.start_date} {item.end_date ? `- ${item.end_date}` : '- Current'}</div>
                          <div className={styles.name}>{item.company_name.text}</div>
                        </div>
                        <div className={styles.rightSide}>
                          <div className={styles.position}>{item.position}</div>
                          <div className={styles.description} dangerouslySetInnerHTML={{ __html: item.description.html }}/>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
              <div className={styles.stacks}>
                {
                  stacks.map((item, index)=>{
                    const stackItems = item.items;
                    return (
                      <div key={index} className={styles.stack}>
                        <h2>{item.primary.stack_title.text}</h2>
                        <div className={styles.images}>
                          {
                            stackItems.map((stackItem, i)=>{
                              return (
                                <div key={i} className={styles.image}>
                                  <Img  fluid={stackItem.logo.localFile.childImageSharp.fluid} alt={stackItem.logo.alt}/>
                                </div>
                              )
                            })
                          }
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </section>
      </Layout>
    </React.Fragment>
  )
}

export default IndexPage
export const homeQuery = graphql`
query homeQuery {
  prismicHomepage {
    data {
      intro {
        html
      }
      featured_image {
        alt
        url
      }
      cv {
        target
        url
      }
      featured_image {
        alt
        localFile {
          childImageSharp {
            fluid(maxWidth: 1200) {
                ...GatsbyImageSharpFluid
            }
          }
        }
      }
      body {
        ... on PrismicHomepageBodyWorkHistory {
          items {
            company_name {
              text
            }
            description {
              html
            }
            position
            start_date(formatString: "YYYY")
            end_date(formatString: "YYYY")
          }
        }
        ... on PrismicHomepageBodyFrontend {
          primary {
            stack_title {
              text
            }
          }
          items {
            logo {
              url
              alt
              localFile {
                childImageSharp {
                  fluid(maxWidth: 128) {
                      ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
`
