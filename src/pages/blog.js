import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"
import SEO from "../components/seo"
import Layout from "../layouts/layout"
import styles from "../styles/blog.module.scss"

const BlogPage = ({ data }) => {
  const posts = data.allPrismicPost.edges.filter((post)=>{
    return post.node.uid !== 'template';
  });
    return (
      <Layout>
        <SEO title="blog" canonicalLink="https://iamcollins.dev/blog" description="Blog page containg a list of all blog posts"/>
        <section className={`${styles.page} page`}>
          <h1>Posts</h1>
          <section className={`${styles.content} content`}>
          <ul>
          {
              posts.map((post, index) => {
                  const {uid, first_publication_date:datePosted, data:postData} = post.node;
                  const url = `/blog/${uid}`;
                  return (
                    <li key={index}>
                        <div className={styles.image}>
                          <Link to={url}><Img fluid={postData.featured_image.localFile.childImageSharp.fluid} fadeIn alt={postData.featured_image.alt}/></Link>
                        </div>
                      <div className={styles.info}>
                        <div className={styles.date}>{datePosted}</div>
                        <div className={styles.title}><Link to={url}>{postData.title.text}</Link></div>
                      </div>
                    </li>
                  )
              })
          }
          </ul>
          {posts.length === 0 ? <h2>No posts at this moment</h2> : ''}
          </section>
        </section>
      </Layout>
    )
}

export default BlogPage

export const blogQuery = graphql`
query BlogQuery {
    allPrismicPost(sort:{ fields: [last_publication_date], order: DESC}) {
        edges {
          node {
            uid
            first_publication_date(formatString: "DD MMMM YYYY")
            last_publication_date
            data {
              title {
                text
              }
              content {
                html
              }
              featured_image {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 600) {
                        ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
    }
}
`