import React from "react"
import { graphql, Link } from "gatsby"
import Img from "gatsby-image"
import SEO from "../components/seo"
import Layout from "../layouts/layout"
import styles from "../styles/projects.module.scss"

const ProjectsPage = ({ data }) => {
    const projects = data.allPrismicProject.edges.filter((project)=>{
        return project.node.uid !== 'template';
    })
    return (
        <Layout>
            <SEO title="projects" canonicalLink="https://iamcollins.dev/projects" description="Projects page containg a list of projects"/>
            <section className={`${styles.page} page`}>
                <h1>Projects</h1>
                <section className={`${styles.content} content`}>
                <ul>
                    {
                        projects.map((project, index) => {
                            const {data:projectData, uid} = project.node;
                            const url = `/projects/${uid}`
                            return (
                                <li key={index}>
                                    <Link to={url}>
                                        <div className={styles.content}>
                                            <div className={styles.logo}>
                                                <Img fluid={projectData.logo.localFile.childImageSharp.fluid}/>
                                            </div>
                                            <div className={styles.info}>
                                                <div className={styles.name}>{projectData.name.text}</div>
                                                <div className={styles.role}>Role: {projectData.role}</div>
                                                <p className={styles.description}>{projectData.details.text}</p>
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                            )
                        })
                    }
                </ul>
                {projects.length === 0 ? <h2>No projects at this moment</h2> : ''}
                </section>
            </section>
        </Layout>
    )
}

export default ProjectsPage

export const projectsQuery = graphql`
query ProjectsQuery {
    allPrismicProject(sort:{ fields: [last_publication_date], order: DESC}) {
        edges {
            node {
            uid
            data {
                name {
                    text
                }
                demo_link {
                    url
                    target
                }
                repo_link {
                    url
                    target
                }
                details {
                    text
                }
                role
                logo {
                    localFile {
                        childImageSharp {
                            fluid(maxWidth: 1200) {
                                ...GatsbyImageSharpFluid
                            }
                        }
                    }
                }
            }
            }
        }
    }
}
`