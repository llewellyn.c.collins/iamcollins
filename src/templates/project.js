import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import SEO from "../components/seo"
import Layout from "../layouts/layout"
import styles from "../styles/project.module.scss"

const Project = ({ data: { prismicProject } }) => {
  const { data, uid } = prismicProject;
  const { stack } = data;
  return (
    <Layout>
      <SEO title={data.name.text} canonicalLink={`https://iamcollins.dev/projects/${uid}`} description="A project page, containing all the project information"/>
      <section className={`${styles.page} page`}>
      <h1>{data.name.text}</h1>
      <article className={styles.article}>
          <h2>Description</h2>
          <p dangerouslySetInnerHTML={{ __html: data.details.html }}/>
        </article>
      <section className={styles.meta}>
        <section className={styles.links}>
          <h2>Links</h2>
          <div>
            {
              data && data.demo_link && data.demo_link.url ? <a href={data.demo_link.url} target="_blank" rel="noopener noreferrer" >Demo</a> : null
            }

            {
              data && data.repo_link && data.repo_link.url ? <a href={data.repo_link.url} target="_blank" rel="noopener noreferrer" >Repository</a> : null
            }
          </div>
        </section>
        <section className={styles.stack}>
          <h2>Stack</h2>
          <div className={styles.images}>
            {
              stack.map((data, index)=>{
                return <Img fluid={data.tech.localFile.childImageSharp.fluid} key={index} alt={data.tech.alt}/>
              })
            }
          </div>
        </section>
      </section>
      </section>
    </Layout>
  )
}

export default Project

export const pageQuery = graphql`
  query ProjectBySlug($uid: String!) {
    prismicProject(uid: { eq: $uid }) {
      uid
      data {
        name {
          text
        }
        demo_link {
          url
          target
        }
        repo_link {
          url
          target
        }
        details {
          html
        }
        stack {
          tech {
            alt
            localFile {
              childImageSharp {
                fluid(maxWidth: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`