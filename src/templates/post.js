import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../layouts/layout"
import styles from "../styles/post.module.scss"

import twitterImage from "../images/twitter.png"
import linkedInImage from "../images/linkedin.png"
import redditImage from "../images/reddit.png"

const Post = ({ data: { prismicPost } }) => {
  const { data:postData, first_publication_date: datePosted, uid } = prismicPost;
  const title = postData.title.text;
  const fullUrl = `https://iamcollins.dev/blog/${uid}`;

  const twitterShareText = `${title} by @iamcollinsZA. ${fullUrl}`;
  const linkedInShareText = `${title} by Llewellyn Collins`;
  return (
    <Layout>
      <SEO title={title} canonicalLink={fullUrl} description="A blog article"/>
      
      <article className={`${styles.post}`}>
          <h1>{title}</h1>
          <section className={styles.content} dangerouslySetInnerHTML={{ __html: postData.content.html }} />
          <footer className={styles.footer}>
            <div className={styles.info}>{datePosted} | Llewellyn Collins</div>
            <div className={styles.social}>
              <a className="twitter-share-button"
                  href={`https://twitter.com/intent/tweet?text=${twitterShareText}`}
                  target="_blank" rel="noopener noreferrer">
                    <img src={twitterImage} className={styles.twitter} alt="Twitter Icon"/>
              </a>
              <a href={`https://www.linkedin.com/shareArticle?mini=true&url=${fullUrl}&title=${title}&summary=${linkedInShareText}`} target="_blank" rel="noopener noreferrer" > 
                <img src={linkedInImage} className={styles.linkedIn} alt="LinkedIn Icon"/>
              </a>
              <a href={`http://www.reddit.com/submit?url=${fullUrl}&title=${title}`} target="_blank" rel="noopener noreferrer">
                <img src={redditImage} className={styles.reddit} alt="Reddit Icon"/>
              </a>
            </div>
          </footer>
      </article>
    </Layout>
  )
}

export default Post

export const pageQuery = graphql`
  query PostBySlug($uid: String!) {
    prismicPost(uid: { eq: $uid }) {
      uid
      first_publication_date(formatString: "DD MMMM YYYY")
      data {
        title {
          text
        }
        content {
          html
        }
      }
    }
  }
`