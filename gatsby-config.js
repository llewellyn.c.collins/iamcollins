require( `dotenv` ).config( {
    path: `.env.${process.env.NODE_ENV}`
} );

module.exports = {
    siteMetadata: {
        title      : `iamcollins`,
        description: `Llewellyn Collins personal website`,
        author     : `@iamcollinsZA`,
        canonicalLink    : 'https://iamcollins.dev',
        siteUrl: 'https://iamcollins.dev',
    },
    plugins: [
        `gatsby-plugin-sass`,
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-prismic`,
            options: {
                repositoryName: `iamcollins`,
                accessToken   : `${process.env.API_KEY}`,
                linkResolver  : ( { node, key, value } ) => post => `/${post.uid}`
            }
        },
        {
            resolve: `gatsby-plugin-prefetch-google-fonts`,
            options: {
                fonts: [
                    {
                        family: `Raleway`
                    }
                ]
            }
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`
            }
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name            : `iamcollins`,
                short_name      : `iamcollins`,
                start_url       : `/`,
                background_color: `#222f3e`,
                theme_color     : `#e84118`,
                display         : `standalone`,
                icon            : `src/images/icon.jpg`
            }
        },
        `gatsby-plugin-offline`,
        `gatsby-plugin-sitemap`,
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
              trackingId: `UA-127934983-3`
            }
        },
        `gatsby-plugin-netlify`
    ]
}
