/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require( `path` )

exports.createPages = async( { graphql, actions } ) => {
    const { createPage } = actions;
    let posts = []
    let projects = [];

    posts = await graphql( `
    {
      allPrismicPost {
        edges {
          node {
            uid
          }
        }
      }
    }
  ` ).catch((error)=>{
      console.error(error);
    })

    projects = await graphql( `
    {
      allPrismicProject {
        edges {
          node {
            uid
          }
        }
      }
    }
  ` ).catch((error)=>{
      console.error(error);
    })

    const postsTemplate = path.resolve( `src/templates/post.js` )
    const projectsTemplate = path.resolve( `src/templates/project.js` )

    posts.data.allPrismicPost.edges.forEach( edge => {
      if(edge.node.uid !== 'template'){
          createPage( {
            path     : `/blog/${edge.node.uid}`,
            component: postsTemplate,
            context  : {
                uid: edge.node.uid
            }
        } )
      }
    } )

    projects.data.allPrismicProject.edges.forEach( edge => {
      if(edge.node.uid !== 'template'){
        createPage( {
            path     : `/projects/${edge.node.uid}`,
            component: projectsTemplate,
            context  : {
                uid: edge.node.uid
            }
        } )
      }
    } )
}
